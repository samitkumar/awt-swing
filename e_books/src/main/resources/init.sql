CREATE DATABASE test;
USE test;
CREATE TABLE ebooks (
    book varchar(255) NOT NULL PRIMARY KEY
);

INSERT INTO ebooks (book) VALUES ("c");
INSERT INTO ebooks (book) VALUES ("java");
INSERT INTO ebooks (book) VALUES ("html");

CREATE TABLE ebooks_name (
    book varchar(255),
    book_name varchar(255) NOT NULL,
    FOREIGN KEY (book) REFERENCES ebooks(book)
);

INSERT INTO ebooks_name (book,book_name) VALUES ("c","ADVANCE C");
INSERT INTO ebooks_name (book,book_name) VALUES ("c","programming with c");
INSERT INTO ebooks_name (book,book_name) VALUES ("c","C basic");