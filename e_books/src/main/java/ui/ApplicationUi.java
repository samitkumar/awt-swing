package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import database.DataAccessLayer;
import database.MockData;

public class ApplicationUi {

	private JFrame frmEBooksFinder;
	private JTextField textField;
	private JList<String> resultList;
	private String dropDownValue;
	private String searchBoxValue;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ApplicationUi window = new ApplicationUi();
					window.frmEBooksFinder.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ApplicationUi() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmEBooksFinder = new JFrame();
		frmEBooksFinder.getContentPane().setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		frmEBooksFinder.setFont(new Font("Dialog", Font.PLAIN, 16));
		frmEBooksFinder.setTitle("e Books Finder");
		frmEBooksFinder.setBounds(100, 100, 585, 525);
		frmEBooksFinder.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEBooksFinder.getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("e Books Finder");
		lblNewLabel.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		lblNewLabel.setBounds(56, 31, 292, 32);
		frmEBooksFinder.getContentPane().add(lblNewLabel);

		JLabel lblPleaseSelectA = new JLabel("Please Select A Subject For e Books Library");
		lblPleaseSelectA.setBounds(56, 78, 305, 23);
		frmEBooksFinder.getContentPane().add(lblPleaseSelectA);

		final JComboBox comboBox = new JComboBox();
		loadAvailableSubject(comboBox);

		comboBox.setBounds(56, 112, 158, 32);
		frmEBooksFinder.getContentPane().add(comboBox);

		JButton btnNewButton = new JButton("Search");
		btnNewButton.setBounds(215, 113, 117, 29);
		frmEBooksFinder.getContentPane().add(btnNewButton);

		JLabel lblNewLabel_1 = new JLabel("Or Please write the subject in the box");
		lblNewLabel_1.setBounds(56, 165, 274, 23);
		frmEBooksFinder.getContentPane().add(lblNewLabel_1);

		textField = new JTextField();
		textField.setBounds(56, 200, 149, 26);
		frmEBooksFinder.getContentPane().add(textField);
		textField.setColumns(10);

		JButton btnNewButton_1 = new JButton("Search");
		btnNewButton_1.setBounds(230, 200, 117, 29);
		frmEBooksFinder.getContentPane().add(btnNewButton_1);

		final Label lblNewLabel_2 = new Label();
		lblNewLabel_2.setBounds(56, 245, 310, 32);
		frmEBooksFinder.getContentPane().add(lblNewLabel_2);

		// popup

		final Label label = new Label();
		label.setBounds(56, 296, 310, 32);
		frmEBooksFinder.getContentPane().add(label);

		final DefaultListModel<String> l1 = new DefaultListModel();
		resultList = new JList(l1);
		resultList.setBounds(56, 345, 244, 120);
		resultList.setVisible(false);
		frmEBooksFinder.getContentPane().add(resultList);

		// open operation
		final JButton openBtn = new JButton("open");
		openBtn.setBounds(312, 340, 117, 29);
		openBtn.setVisible(false);
		frmEBooksFinder.getContentPane().add(openBtn);

		// OPEN button action for dropdown
		openBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (resultList.getSelectedIndex() != -1) {
					String subject = "";
					if (dropDownValue != null) {
						subject = dropDownValue;
					} else {
						subject = searchBoxValue;
					}
					openBook(resultList.getSelectedValue(), subject);
				}
			}
		});

		// SEARCH1 button action for dropdown
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dropDownValue = (String) comboBox.getItemAt(comboBox.getSelectedIndex());
				textField.setText("");
				if ("Please Select..".equalsIgnoreCase(dropDownValue)) {
					alertMe("info", "Invalid Search!..");
				} else {
					lblNewLabel_2.setText("The Available e Books for : " + dropDownValue);
					searchResult(resultList, l1, label, dropDownValue, openBtn);
				}
			}
		});

		// SEARCH2 button action for textbox
		btnNewButton_1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				searchBoxValue = textField.getText();
				loadAvailableSubject(comboBox);
				if ("".equalsIgnoreCase(searchBoxValue)) {
					alertMe("info", "Invalid Search!..");
					// do nothing
				} else {
					lblNewLabel_2.setText("The Available e Books for : " + searchBoxValue);
					searchResult(resultList, l1, label, searchBoxValue, openBtn);
				}
			}
		});
	}

	/**
	 * 
	 * @param table
	 * @param label
	 */
	protected void searchResult(JList<String> resultList, DefaultListModel<String> l1, Label label, String subject,
			JButton openBtn) {
		List<String> databaseData = new DataAccessLayer().getAvailableBooksForSubject(subject);
		if (databaseData != null && !databaseData.isEmpty()) {
			for (String val : databaseData) {
				l1.addElement(val);
			}
			label.setText("Select and click on open to read the book ");
			resultList.setVisible(true);
			openBtn.setVisible(true);
		} else {
			resultList.setVisible(false);
			openBtn.setVisible(false);
			label.setText("No Result Found! ");
		}

	}

	/**
	 * 
	 * @param comboBox
	 */
	private void loadAvailableSubject(JComboBox comboBox) {
		String[] dropDownData = new DataAccessLayer().getAvailableBooks();
		if (dropDownData == null) {
			dropDownData = MockData.DROPDOWN;
		}
		comboBox.setModel(new DefaultComboBoxModel(dropDownData));
	}

	private void alertMe(String title, String msg) {
		JOptionPane.showMessageDialog(null, msg, title, JOptionPane.WARNING_MESSAGE);
	}

	private void openBook(String book, String subject) {
		/*
		 * JFrame myFrame = new JFrame(subject + "("+book+")");
		 * myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 * myFrame.setSize(100, 100); JEditorPane myPane = new JEditorPane();
		 * myPane.setContentType("text/plain"); myPane.setText(new
		 * DataAccessLayer().getMockedBookDataForSubject(subject));
		 * myFrame.setContentPane(myPane); myFrame.setVisible(true);
		 */

		JFrame frame = new JFrame(subject + "(" + book + ")");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container cp = frame.getContentPane();
		JTextPane pane = new JTextPane();
		SimpleAttributeSet attributeSet = new SimpleAttributeSet();
		StyleConstants.setBold(attributeSet, true);

		// Set the attributes before adding text
		pane.setCharacterAttributes(attributeSet, true);
		pane.setText(new DataAccessLayer().getMockedBookDataForSubject(subject));

		StyleConstants.setItalic(attributeSet, true);
		StyleConstants.setForeground(attributeSet, Color.red);
		StyleConstants.setBackground(attributeSet, Color.blue);

		JScrollPane scrollPane = new JScrollPane(pane);
		cp.add(scrollPane, BorderLayout.CENTER);

		frame.setSize(500, 450);
		frame.setVisible(true);
	}
}
