package database;

public class Ebooks {
	private String booksName;
	private String authorName;
	private String dateOfPubliction;
	private String read;
	private String download;
	public String getBooksName() {
		return booksName;
	}
	public void setBooksName(String booksName) {
		this.booksName = booksName;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public String getDateOfPubliction() {
		return dateOfPubliction;
	}
	public void setDateOfPubliction(String dateOfPubliction) {
		this.dateOfPubliction = dateOfPubliction;
	}
	public String getRead() {
		return read;
	}
	public void setRead(String read) {
		this.read = read;
	}
	public String getDownload() {
		return download;
	}
	public void setDownload(String download) {
		this.download = download;
	}
	
}
