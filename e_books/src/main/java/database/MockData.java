package database;

import java.util.Arrays;
import java.util.List;

public class MockData {
	
	public static String[] DROPDOWN = {"Please Select..", "C", "C++", "Java", "Html", "Java Script", "Other.."} ;
	
	public static List<String> C_l = Arrays.asList("Ansible C","C Geneous","C Basic","C in details","C in Action");
	public static String[][] C = { 
			{ "1.", "Ansible C", "download/read" }, 
			{ "2.", "C Geneous", "download/read" },
			{ "3.", "C Basic", "download/read" }, 
			{ "4.", "C in details", "download/read" }, 
			{ "5.", "C in Action", "download/read" } 
		};
	public static String C_data = "C belongs to the structured, procedural paradigms of languages. It is proven, flexible and powerful and may be used for a variety of different applications. Although high-level, C and assembly language share many of the same attributes.\n" +
            "\n" +
            "Some of C's most important features include:\n" +
            "\n" +
            "Fixed number of keywords, including a set of control primitives, such as if, for, while, switch and do while\n" +
            "Multiple logical and mathematical operators, including bit manipulators\n" +
            "Multiple assignments may be applied in a single statement.\n" +
            "Function return values are not always required and may be ignored if unneeded.\n" +
            "Typing is static. All data has type but may be implicitly converted.\n" +
            "Basic form of modularity, as files may be separately compiled and linked\n" +
            "Control of function and object visibility to other files via extern and static attributes" ;
	
	public static List<String> C_PLUS_PUS_l = Arrays.asList("C++ for dummies","Effective C++","Practical C++ Programming","C++ fo Kids");
	public static String[][] C_PLUS_PUS = { 
			{ "1.", "C++ for dummies", "download/read" }, 
			{ "2.", "Effective C++", "download/read" },
			{ "3.", "Practical C++ Programming", "download/read" }, 
			{ "4.", "C++ fo Kids", "download/read" }
		};
	public static String C_PLUS_PUS_data = "C++ (pronounced /ˌsiːˌplʌsˈplʌs/ \"cee plus plus\") is a general-purpose programming language. It has imperative, object-oriented and generic programming features, while also providing facilities for low-level memory manipulation.\n" +
	        "\n" +
	        "It was designed with a bias toward system programming and embedded, resource-constrained and large systems, with performance, efficiency and flexibility of use as its design highlights.[7] C++ has also been found useful in many other contexts, with key strengths being software infrastructure and resource-constrained applications,[7] including desktop applications, servers (e.g. e-commerce, web search or SQL servers), and performance-critical applications (e.g. telephone switches or space probes).[8] C++ is a compiled language, with implementations of it available on many platforms. Many vendors provide C++ compilers, including the Free Software Foundation, Microsoft, Intel, and IBM.\n" +
	        "\n" +
	        "C++ is standardized by the International Organization for Standardization (ISO), with the latest standard version ratified and published by ISO in December 2017 as ISO/IEC 14882:2017 (informally known as C++17).[9] The C++ programming language was initially standardized in 1998 as ISO/IEC 14882:1998, which was then amended by the C++03, C++11 and C++14 standards. The current C++17 standard supersedes these with new features and an enlarged standard library. Before the initial standardization in 1998, C++ was developed by Bjarne Stroustrup at Bell Labs since 1979, as an extension of the C language as he wanted an efficient and flexible language similar to C, which also provided high-level features for program organization. C++20 is the next planned standard thereafter.";
	
	public static List<String> JAVA_l = Arrays.asList("Head Fast Java","Effective Java","Thinking In Java","Learning Java","Core Java","Java Concurrency","Student Workbook");
	public static String[][] JAVA = { 
			{ "1.", "Head Fast Java", "download/read" }, 
			{ "2.", "Effective Java", "download/read" },
			{ "3.", "Thinking In Java", "download/read" }, 
			{ "4.", "Learning Java", "download/read" },
			{ "5.", "Core Java", "download/read" },
			{ "6.", "Java Concurrency", "download/read" },
			{ "7.", "Student Workbook", "download/read" }
		};
	public static String JAVA_data = "Java is a general purpose, high-level programming language developed by Sun Microsystems. The Java programming language was developed by a small team of engineers, known as the Green Team, who initiated the language in 1991.\n" +
            "\n" +
            "The Java language was originally called OAK, and at the time it was designed for handheld devices and set-top boxes. Oak was unsuccessful and in 1995 Sun changed the name to Java and modified the language to take advantage of the burgeoning World Wide Web.\n" +
            "\n" +
            "Later, in 2009, Oracle Corporation acquired Sun Microsystems and took ownership of two key Sun software assets: Java and Solaris.";
	
	public static List<String> JAVASCRIPT_l = Arrays.asList("Speaking JavaScript","JavaScript and Jquery","Learnign Javascript","Scope and Closures");
	public static String[][] JAVASCRIPT = { 
			{ "1.", "Speaking JavaScript", "download/read" }, 
			{ "2.", "JavaScript and Jquery", "download/read" },
			{ "3.", "Learnign Javascript", "download/read" }, 
			{ "4.", "Scope and Closures", "download/read" }
		};
	public static String JAVASCRIPT_data = "JavaScript is a client-side scripting language, which means the source code is processed by the client's web browser rather than on the web server. This means JavaScript functions can run after a webpage has loaded without communicating with the server. For example, a JavaScript function may check a web form before it is submitted to make sure all the required fields have been filled out. The JavaScript code can produce an error message before any information is actually transmitted to the server.\n" +
            "\n" +
            "Like server-side scripting languages, such as PHP and ASP, JavaScript code can be inserted anywhere within the HTML of a webpage. However, only the output of server-side code is displayed in the HTML, while JavaScript code remains fully visible in the source of the webpage. It can also be referenced in a separate .JS file, which may also be viewed in a browser.";
	
	public static List<String> HTML_l = Arrays.asList("html5","html & css","Webdesign with html"); 
	public static String[][] HTML = { 
			{ "1.", "html5", "download/read" }, 
			{ "2.", "html & css", "download/read" },
			{ "3.", "Webdesign with html", "download/read" }
		};
	public static String HTML_data = "HTML (Hypertext Markup Language)\n" +
            "\n" +
            "Posted by: Margaret Rouse\n" +
            "WhatIs.com\n" +
            "   \n" +
            "Contributor(s): Andrew Trubac\n" +
            "\n" +
            "Sponsored News\n" +
            "What’s Happening in Complex and Always-On Data–And Why You Need a New Approach\n" +
            "–SAS Institute Inc.\n" +
            "5 Ways Technology Will Boost Enterprise Productivity in 2017\n" +
            "–DellEMC\n" +
            "See More\n" +
            "\n" +
            "Vendor Resources\n" +
            "JavaScript – 20 Lessons to Successful Web Development\n" +
            "–ComputerWeekly.com\n" +
            "HTML (Hypertext Markup Language) is the set of markup symbols or codes inserted in a file intended for display on a World Wide Web browser page. The markup tells the Web browser how to display a Web page's words and images for the user. Each individual markup code is referred to as an element (but many people also refer to it as a tag). Some elements come in pairs that indicate when some display effect is to begin and when it is to end.\n" +
            "\n" +
            "HTML is a formal Recommendation by the World Wide Web Consortium (W3C) and is generally adhered to by the major browsers, Microsoft's Internet Explorer and Netscape's Navigator, which also provide some additional non-standard codes. The current version of HTML is html5";
	

	public String[][] getSomething(){
		String[][] blabla = new String[4][4];
		for(int i=0 ; i<4 ; i++){
			for(int j=0 ; j<4;j++){
				blabla[i][j] = "_"+i+"_"+j;
			}
		}
		
		return blabla;
	}
}
