package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DataAccessLayer {

	public Connection getConnection() {
		final String CONNECTION_STRING = "jdbc:mysql://localhost:3306/test";
		final String USER_NAME = "root";
		final String PASSWORD = "root";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection(CONNECTION_STRING, USER_NAME, PASSWORD);
			return con;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	public String[][] readDataFromDatabase() {
		return null;
	}

	public void createTableIfNotAvailable() {

	}

	public String[] getAvailableBooks() {
		final List<String> books = new ArrayList<String>();
		try {
			Connection con = getConnection();
			if (con != null) {
				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery("select * from ebooks");
				books.add("Please Select..");
				while (rs.next())
					books.add(rs.getString(1));
				con.close();
				
				return books.toArray(new String[books.size()]);
			} else {
				System.out.println("connection is still null");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}
	
	
	
	public List<String> getAvailableBooksForSubject(String subject) {
		final List<String> allBooks = new ArrayList<String>();
		try {
			Connection con = getConnection();
			if (con != null) {
				Statement stmt = con.createStatement();
				String statement = "SELECT book_name FROM ebooks_name WHERE book = '"+subject + "'";
				System.out.println(statement);
				ResultSet rs = stmt.executeQuery(statement);
				
				while (rs.next())
					allBooks.add(rs.getString("book_name"));
				con.close();
				
				return allBooks;
			} else {
				System.out.println("database connection is still null");
				return getMockedEbooksForSubject(subject);
			}
		} catch (Exception e) {
			System.out.println("Exception :" +  e);
			return getMockedEbooksForSubject(subject);
		}
	}

	public List<String> getMockedEbooksForSubject(String subjectName) {
		if ("C++".equalsIgnoreCase(subjectName)) {
			return MockData.C_PLUS_PUS_l;
		} else if ("C".equalsIgnoreCase(subjectName)) {
			return MockData.C_l;
		} else if ("java".equalsIgnoreCase(subjectName)) {
			return MockData.JAVA_l;
		} else if ("java script".equalsIgnoreCase(subjectName)) {
			return MockData.JAVASCRIPT_l;
		} else if ("html".equalsIgnoreCase(subjectName)) {
			return MockData.HTML_l;
		} else {
			return null;
		}
	}
	
	public String getMockedBookDataForSubject(String subjectName) {
		if ("C++".equalsIgnoreCase(subjectName)) {
			return MockData.C_PLUS_PUS_data;
		} else if ("C".equalsIgnoreCase(subjectName)) {
			return MockData.C_data;
		} else if ("java".equalsIgnoreCase(subjectName)) {
			return MockData.JAVA_data;
		} else if ("java script".equalsIgnoreCase(subjectName)) {
			return MockData.JAVASCRIPT_data;
		} else if ("html".equalsIgnoreCase(subjectName)) {
			return MockData.HTML_data;
		} else {
			return null;
		}
	}
	
}
