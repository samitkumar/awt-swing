var groceries = ['Rice','Dal','Oil','Veg','Meat'];
var stationery = ['Adhesives','Clips','Boards','Books','Clips'];
var kids_accessories = ['football','cycle','ball','bat'];
var electronics = ['TV','Radio','CD','Bulb','Tube'];

function displayList(topic) {

    switch(topic){
        case 'groceries' :
            document.getElementById("list-to-purchase").innerHTML=giveMeDataWithHtml('groceries');
            break;
        case 'stationery' :
            document.getElementById("list-to-purchase").innerHTML=giveMeDataWithHtml('stationery');
            break;
        case 'kids_accessories' :
            document.getElementById("list-to-purchase").innerHTML=giveMeDataWithHtml('kids_accessories');
            break;
        case 'electronics' :
            document.getElementById("list-to-purchase").innerHTML=giveMeDataWithHtml('electronics');
            break;
        default :
            document.getElementById("list-to-purchase").innerText="NO SEARCH FOUND!";
    }
    
}

function giveMeDataWithHtml(topic) {
    let parent = "<ul>";
    let end_parent = "</ul>";
    let result = parent
    switch(topic) {
        case 'groceries' :
            for(i=0;i<groceries.length;i++){
                result += prepareHtml(groceries[i]) ;
            }
            result += end_parent;
            return result;
            break;
        case 'stationery' :
            for(i=0;i<stationery.length;i++){
                result += prepareHtml(stationery[i]) ;
            }
            result += end_parent;
            return result;
            break;
        
        case 'kids_accessories' :
            for(i=0;i<kids_accessories.length;i++){
                result += prepareHtml(kids_accessories[i]) ;
            }
            result += end_parent;
            return result;
            break;

        case 'electronics' :
            for(i=0;i<electronics.length;i++){
                result += prepareHtml(electronics[i]) ;
            }
            result += end_parent;
            return result;
            break;
        default : 
            return "<h4>no result found </h4>";
    }
}

function prepareHtml(val) {
   return "<input type='checkbox' class='select_item' name="+val+" value="+val+">"+val+"<br/>"
}